package com.example.myapplication

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import com.google.android.material.bottomnavigation.BottomNavigationView
import kotlinx.android.synthetic.main.activity_main.*
import ru.terrakok.cicerone.Navigator
import ru.terrakok.cicerone.android.support.SupportAppNavigator
import ru.terrakok.cicerone.commands.Command
import ru.terrakok.cicerone.commands.Replace

class MainActivity : AppCompatActivity(), BottomNavigationView.OnNavigationItemSelectedListener {
    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        when(item.itemId){
            R.id.navigation_home -> {
                AppApplication.app.getRouter().navigateTo(NavigationKeys.First())
            }

            R.id.navigation_dashboard -> {
                AppApplication.app.getRouter().navigateTo(NavigationKeys.Second())
            }
            else -> {
                return false
            }
        }
        return true
    }

    private lateinit var navigator: Navigator

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        this.navigator = SupportAppNavigator(this, R.id.mainContainer)

        if (savedInstanceState == null) {
            navigator.applyCommands(arrayOf<Command>(Replace(NavigationKeys.First())))
        }

        bottom_navigation.setOnNavigationItemSelectedListener { onNavigationItemSelected(it) }
    }

    override fun onPause() {
        super.onPause()
        AppApplication.app.getNavigatorHolder().removeNavigator()
    }

    override fun onResume() {
        super.onResume()
        AppApplication.app.getNavigatorHolder().setNavigator(this.navigator)
    }

    override fun onDestroy() {
        super.onDestroy()
        AppApplication.app.getRouter().exit()
    }
}
