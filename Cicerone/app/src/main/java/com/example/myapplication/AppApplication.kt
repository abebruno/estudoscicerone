package com.example.myapplication

import android.app.Application
import ru.terrakok.cicerone.Cicerone
import ru.terrakok.cicerone.NavigatorHolder
import ru.terrakok.cicerone.Router

/**
* Estou com preguiça de mudar, mas faz mais sentido o cicerone ficar na
 * Activity. Nem precisa dessa application, nesse caso, só cria tudo na
 * main e boa!
* */
class AppApplication: Application() {
    private lateinit var cicerone: Cicerone<Router>

    companion object{
        lateinit var app: AppApplication
    }

    override fun onCreate() {
        super.onCreate()
        app = this
        this.createCicerone()
    }

    private fun createCicerone(){
        this.cicerone = Cicerone.create()
    }

    fun getNavigatorHolder(): NavigatorHolder{
        return this.cicerone.navigatorHolder
    }

    fun getRouter(): Router{
        return this.cicerone.router
    }
}